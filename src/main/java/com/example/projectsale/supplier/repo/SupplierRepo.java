package com.example.projectsale.supplier.repo;

import com.example.projectsale.supplier.entity.Supplier;
import com.example.projectsale.utils.AbstractRepository;

import java.util.UUID;

public interface SupplierRepo extends AbstractRepository<Supplier, UUID> {
}
